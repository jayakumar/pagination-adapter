package com.cycloides.paginationadapter.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.cycloides.paginationadapter.R;
import com.cycloides.paginationlibrary.PaginationAdapter;

import java.util.ArrayList;

/**
 * Created by Jinu on 7/5/18.
 **/
public class TestAdapter extends PaginationAdapter<TestAdapter.MyViewHolder, String> {


    public TestAdapter(ArrayList<String> list, PaginationListener listener) {
        super(list, listener);
    }


    @Override
    public MyViewHolder onCreateVH(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindVH(@NonNull MyViewHolder holder, int position, String item) {
        holder.textView.setText(item);
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        MyViewHolder(View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView);
        }
    }
}
