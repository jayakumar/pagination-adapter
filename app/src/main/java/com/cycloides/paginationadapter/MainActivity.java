package com.cycloides.paginationadapter;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.cycloides.paginationadapter.adapter.TestAdapter;
import com.cycloides.paginationlibrary.PaginationAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements PaginationAdapter.PaginationListener {


    private TestAdapter adapter;
    private ArrayList<String> data = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView = findViewById(R.id.loadingRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new TestAdapter(data, this);
        recyclerView.setAdapter(adapter);
        onLoadMore(data.size());
    }

    @Override
    public void onLoadMore(int pos) {
        Runnable runnable = new Runnable() {
            public void run() {
                updateData(data.size());
            }
        };
        new Handler().postDelayed(runnable, 1000);
    }

    private void updateData(int pos) {
        for (int i = 1; i <= 15; i++) {
            data.add(String.valueOf(pos + i));
        }
        adapter.showNewItems();
        if (data.size()>=120){
            adapter.setFinishedLoading();
        }
    }
}
