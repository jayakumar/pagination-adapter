**Pagination Adapter**<br/>
---------------------------

This adapter helps you to build pagination for android recycler view with some simple steps

<br/>
First of all you have to create an adapter witch extends **PaginationAdapter**



  ```java
    class TestAdapter extends PaginationAdapter<TestAdapter.MyViewHolder, String> {}
  ```
  
  on above code replace **String** with your model object and override and create default constructors
  
  
  On your **Fragment/Activity** create global arraylist variable for the list and impliment the interface for pagination
  
  ```java
     private ArrayList<String> data = new ArrayList<>();
    
     //pass this variable to the adapter constructor
     adapter = new TestAdapter(data, this);
     
        
  ```
  once you got call on **onLoadMore** interface call api for tetching new data
  and once you got new data add new items in the array call **showNewItems()**
  ```java
     data.addAll(newItems);
     adapter.showNewItems();
  ```
  
  And finally you have loaded all items from api use
  ```java
     adapter.setFinishedLoading();
  ```